package co.simplon.promo18;

import java.text.MessageFormat;
import java.util.LinkedList;

public class Fight {

  private LinkedList<Gladiator> initiativeOrder = new LinkedList<Gladiator>();
  private boolean hasBeenWon;

  private Gladiator winner;

  public LinkedList<Gladiator> getInitiativeOrder() {
    return initiativeOrder;
  }

  public String getInitiativeOrderAsString() {
    String message = MessageFormat.format("Le premier combattant à s'élancer est {0}, le second est {1}",
        initiativeOrder.get(0), initiativeOrder.get(1));
    return message;
  }

  public Gladiator getWinner() {
    return winner;
  }

  // TODO extract into method, ctor will build initiative himself so no need to
  // pass to it (via Team-s?)
  public Fight(Gladiator glad1, Gladiator glad2) {
    // random toss to get initiative
    double toss = (Math.random());
    // System.out.println(toss > 0.5 ? "pile" : "face");

    if (toss > 0.5) {
      initiativeOrder.add(glad1);
      initiativeOrder.add(glad2);
    } else {
      initiativeOrder.add(glad2);
      initiativeOrder.add(glad1);
    }

  }

  // static?
  private void dealDamage(Gladiator source, Gladiator target) {
    // get source STR
    int sourceSTR = source.getStrength();
    // get target DEF
    int targetDEF = target.getDefense();
    
    // reduce stamina
    source.removeStamina(1);

    // wound
    // damage = STR - DEF (with small randomness for both)
    int damage = sourceSTR - targetDEF;
    // target HP -= damage
    target.removeHP(damage);

  }

  private void processTurn() {
    Gladiator protagonist = initiativeOrder.pop();

    Gladiator antagonist = initiativeOrder.pop();

    System.out.println(antagonist.getHP());
    String message = MessageFormat.format("{0}(STR={1}) attacks {2}(DEF={3})",
        protagonist.getNomen(), protagonist.getStrength(), antagonist.getNomen(), antagonist.getDefense());
    System.out.println(message);

    message = MessageFormat.format("{0}: {1}hp", antagonist.getNomen(), antagonist.getHP());
    System.out.println(message);

    // est-ce qu'on implémente le stamina dans dealDamage ou après?
    this.dealDamage(protagonist, antagonist);

    message = MessageFormat.format("{0}: {1}hp", antagonist.getNomen(), antagonist.getHP());
    System.out.println(message);

    initiativeOrder.push(protagonist);
    initiativeOrder.push(antagonist);

  }

  private void checkWorld() {
    for (Gladiator opponent : initiativeOrder) {
      // stamina check
      if (opponent.getStamina() <= 0) {
        opponent.setDefense(0);
      }

      // naive implementation, only works for 2 opponents
      if (!opponent.isAlive()) {
        hasBeenWon = true;

        // remove loser from initiativeOrder, remaining Gladiator is the winner
        initiativeOrder.remove(opponent);
        this.winner = initiativeOrder.pop();
      }

    }
  }

  public void resolve() {

    while (!hasBeenWon) {
      processTurn();
      checkWorld();

    }

  }

}
