package co.simplon.promo18;

import java.io.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;

// from https://koor.fr/Java/Tutorial/java_design_patterns_singleton.wp
// see https://gameprogrammingpatterns.com/singleton.html for critical thinking

public final class Resources {

  public ArrayList<String> nomenList = new ArrayList<String>();
  public ArrayList<String> preanomenList = new ArrayList<String>();

  public String[] sounds = { "pif", "paf", "pouf", "bam", "bim", "boum", "splat", "whack" };

  public static final String TOPLEFTCORNER = "┌";
  public static final String TOPRIGHTCORNER = "┐";
  public static final String BOTLEFTCORNER = "└";
  public static final String BOTRIGHTCORNER = "┘";
  public static final String HORLINE = "─";
  public static final String VERTLINE = "│";

  // Singleton Instance
  private static final Resources INSTANCE = new Resources();

  public static Resources getInstance() {
    return INSTANCE;
  }

  // ctor
  private Resources() {
    try {
      loadResourceFromFile(this.nomenList, "nomen.txt");
      loadResourceFromFile(this.preanomenList, "praenomen.txt");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

  }

  private void loadResourceFromFile(List<String> list, String fileName) throws FileNotFoundException {
    InputStream is = getClass().getClassLoader().getResourceAsStream(fileName);

    if (null == is) {
      throw new FileNotFoundException();
    }

    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is))) {
      String line;
      while ((line = bufferedReader.readLine()) != null) {
        list.add(line);
      }

    } catch (IOException e) {
      // Auto-generated catch block
      e.printStackTrace();
    }
  }

  public String getRandomSound() {
    int index = (int) (Math.random() * sounds.length);
    return sounds[index];
  }

  public String getRandomSoundCap() {
    return StringUtils.capitalize(getRandomSound());
  }


  // draw ASCII frames
  public String drawTop() {
    return drawTop(85);
  }

  public String drawBottom() {
    return drawBottom(85);
  }

  public String drawTop(int width) {
    StringBuffer sBuffer = new StringBuffer(width);
    sBuffer.append(TOPLEFTCORNER);
    sBuffer.append(HORLINE.repeat(width - 2));
    sBuffer.append(TOPRIGHTCORNER);
    return sBuffer.toString();
  }

  public String drawBottom(int width) {
    StringBuffer sBuffer = new StringBuffer(width);
    sBuffer.append(BOTLEFTCORNER);
    sBuffer.append(HORLINE.repeat(width - 2));
    sBuffer.append(BOTRIGHTCORNER);
    return sBuffer.toString();
  }
}
