package co.simplon.promo18;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Story {

  private StringBuffer stringBuffer;

  // ctor
  public Story() {
    stringBuffer = new StringBuffer();
  }

  public static void play() {
    // var not used
    String story = """
        YOU ARE XXXX XXXXX
        A gladiator from XXXX, XXXXX
        TODAY YOU FACE (BIG BOSS)
        THE FIGHT STARTS...

        you hit, he blocks
        he hits, you dodge,
        you hit, you miss,
        he hits, he lands !!!!

        You're KNOCKED OUT !

        better luck next time

        some dude takes you in, (he has a grudge or something)
        you start training

        choose what tool you will train with (choice of stat + randomness)

        (training?) stamina decreases

        hand hold the first couple of times

        choose a fight, random for now

        XP / stamina / money bonus?

        lodging/food for stamina



        """;
    Story currentStory = new Story();

    // the player
    Gladiator yourGladiator = new Gladiator();

    String message = MessageFormat.format("YOU ARE {0} {1}",
        yourGladiator.getPraenomen(), yourGladiator.getNomen());
    currentStory.add(message.toUpperCase());

    currentStory.add("a gladiator from LVGDVNVM coming to fight in ARELATE SEXTANORVM");

    Gladiator bigBoss = new Gladiator("Crixus", 50, 100);
    Fight tutorialBossFight = new Fight(yourGladiator, bigBoss);

    message = MessageFormat.format("Today you fight {0}, RENOWNED CHAMPION", bigBoss.getNomen().toUpperCase());
    currentStory.add(message);
    currentStory.add("THE FIGHT STARTS...");

    System.out.println(currentStory.print());
    currentStory.clear();

    // Fight calculate and prints
    tutorialBossFight.resolve();

    System.out.println(Resources.getInstance().drawBottom());
    System.out.println("the winner is " + tutorialBossFight.getWinner().getNomen());

    // bigBoss always wins the first fight
    currentStory.add("you're defeated !!!".toUpperCase());
    currentStory.add("You wait up in a dark room; a master took pity of you and took you in");
    // Training presentation
    currentStory.add("you need to Train!!!");

    System.out.println(currentStory.print());

    currentStory.clear();

    // revive after first fight
    yourGladiator.revive();

    // yourGladiator, new Gladiator()repeat? store state?
    while (yourGladiator.isAlive()) {
      
    // input: choose training tool / strategy
    askToTrain(yourGladiator);

    // choose fight
    System.out.println("now you fight!!!!");
    // askToFight(yourGladiator);
    Fight fight = new Fight(yourGladiator, new Gladiator());
    fight.resolve();
    }

    // you always die someday
    System.out.println("YOU LOSE !!!");
    

  }

  private static void askToTrain(Gladiator yourGladiator) {
    String message;
    message = """
        Your stats: {0} STR:{1} STAM:{2} DEF:{3}
        Choose training:
         1. train Strength
         2. train Stamina
         3. train Defense
         """;

    message = MessageFormat.format(message,
        yourGladiator.getNomen(), yourGladiator.getStrength(), yourGladiator.getStamina(), yourGladiator.getDefense());

    System.out.println(message);

    // input
    boolean valid = false;
    try (
        Scanner sc = new Scanner(System.in)) {
      while (!valid) {
        //

        /*
         Exception in thread "main" java.util.NoSuchElementException
        at java.base/java.util.Scanner.throwFor(Unknown Source)
        at java.base/java.util.Scanner.next(Unknown Source)
        at java.base/java.util.Scanner.nextInt(Unknown Source)
        at java.base/java.util.Scanner.nextInt(Unknown Source)
        at co.simplon.promo18.Story.askToTrain(Story.java:129)
        at co.simplon.promo18.Story.play(Story.java:96)
        at co.simplon.promo18.App.main(App.java:14)
        */
        int input = sc.nextInt();

        switch (input) {
          // STR
          case 1:
            valid = true;
            System.out.println("You train Strength");
            yourGladiator.train("STR");
            break;

          // STAM
          case 2:
            valid = true;
            System.out.println("You train Stamina");
            yourGladiator.train("STAM");
            break;

          // DEF
          case 3:
            valid = true;
            System.out.println("You train Defense");
            yourGladiator.train("DEF");
            break;

          default:
            System.out.println("choisissez une valeur entre 1 et 3");
            break;
        }

      }
    }
  }

  public void add(String string) {
    // here we could guard that the string width is > than 85 chars

    stringBuffer.append(string + System.lineSeparator());
  }

  public void clear() {
    stringBuffer.delete(0, stringBuffer.length());
  }

  public String print() {
    return stringBuffer.toString();
  }

  // not used yet
  @Override
  public String toString() {
    return stringBuffer.toString();
  }

}
