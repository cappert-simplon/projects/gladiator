package co.simplon.promo18;

import java.util.List;

// no static classes in Java: ONLY put static members in here
public class GRandom {

  public static String randomInList(List<String> list) {
    int size = list.size();

    int index = (int) (Math.random() * size);

    return list.get(index);

  }

  /**
   * The nextInt(int max) is used to get a random
   * number between 0 and the number passed in argument, inclusive.
   */
  public static int nextInt(int max) {
    return (int) (Math.random() * max + 1);
  }

  public static int between(int min, int max) {
    return min + (int)(Math.random() * ((max - min) + 1));
  }

  // add small random (ajout d'un pourcentage? proportion? divide by zero?)

}
