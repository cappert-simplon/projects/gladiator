package co.simplon.promo18;

// NOT USED YET
public class Weapon {
  private String typeName;

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  @Override
  public String toString() {
    return "Weapon [typeName=" + typeName + "]";
  }

}
