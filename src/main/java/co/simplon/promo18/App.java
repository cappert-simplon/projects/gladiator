package co.simplon.promo18;

/**
 * Hello world!
 *
 */
public class App {

  // load resources at App load (useless?)
  static Resources resources = Resources.getInstance();

  public static void main(String[] args) {

    Story.play();
  }

  public App() {
  }

}
