package co.simplon.promo18;

import java.text.MessageFormat;

public class Gladiator {

  private static final int DEFAULT_STAMINA = 10;

  /**
   * Name of the Gladiator
   * <p>
   * For men, the nomen was the middle of the tria nomina ("three names"), after
   * the praenomen and before the cognomen. For women, the nomen was often the
   * only name used until the late Republic. For example, three members of gens
   * Julia were Gaius Julius Caesar and his sisters Julia Major and Julia Minor
   * ("Julia the elder" and "Julia the younger").
   */
  private String nomen;

  private String praenomen;
  private Weapon weapon;

  // STATS
  private int hp;
  private int maxHp;
  private int stamina;

  // ABILITIES
  private int strength;
  private int defense;
  private int maxDefense;

  // ctor à refacto; normalement on va appeller le plus précis avec les arguments
  // qu'on veut modifier
  // Gladiator(nomen = getRandomNomen()) etc.
  public Gladiator() {
    super();

    // find random name
    this.buildRandomNomen();
    this.buildRandomPraenomen();

    // fill attributes (STR,DEX, etc)
    this.buildRandomAbilities();

    // calculate stats (HP, Stamina)
    this.buildRandomStats();

  }

  // TODO Refacto pour généraliser et reuse tous les ctors
  public Gladiator(String nomen) {
    super();

    this.nomen = nomen;

    buildRandomPraenomen();

    this.buildRandomStats();
    this.buildRandomAbilities();
  }

  /**
   * public Gladiator(String nomen, int hp)
   * 
   * @param nomen
   * @param hp
   */
  public Gladiator(String nomen, int hp) {
    this(nomen, hp, Gladiator.DEFAULT_STAMINA);
  }

  /**
   * Gladiator(String nomen, int hp, int stamina)
   * 
   * @param nomen
   * @param hp
   * @param stamina
   */
  public Gladiator(String nomen, int hp, int stamina) {
    super();

    this.nomen = nomen;
    this.hp = hp;
    this.stamina = stamina;

    buildRandomAbilities();
  }

  private void buildRandomPraenomen() {
    this.praenomen = GRandom.randomInList(Resources.getInstance().preanomenList);
  }

  private void buildRandomNomen() {
    this.nomen = GRandom.randomInList(Resources.getInstance().nomenList);
  }

  private void buildRandomStats() {
    this.maxHp = GRandom.between(10, 19);
    this.hp = this.maxHp;
  }

  private void buildRandomAbilities() {
    // STR
    this.strength = GRandom.between(10, 15);

    // DEF
    this.maxDefense = GRandom.between(8, 12);
    this.defense = this.maxDefense;
  }

  /** get name of the Gladiator */
  public String getNomen() {
    return this.nomen;
  }

  public String getPraenomen() {
    return this.praenomen;
  }

  public Weapon getWeapon() {
    return weapon;
  }

  public void setWeapon(Weapon weapon) {
    this.weapon = weapon;
  }

  public int getMaxHP() {
    return this.maxHp;
  }

  public int getHP() {
    return this.hp;
  }

  public void setHP(int value) {
    this.hp = value;
  }

  public void removeHP(int value) {
    this.setHP(this.getHP() - value);
  }

  public int getStamina() {
    return stamina;
  }

  public void setStamina(int stamina) {
    this.stamina = stamina;
  }

  public void removeStamina(int value) {
    this.setStamina(this.getStamina() - value);
  }

  public int getStrength() {
    return strength;
  }

  public void setStrength(int strength) {
    this.strength = strength;
  }

  public int getDefense() {
    return defense;
  }

  public void setDefense(int defense) {
    this.defense = defense;
  }

  public boolean isAlive() {
    return this.getHP() > 0;
  }

  public void revive() {
    this.hp = this.maxHp;
    this.defense = this.maxDefense;
    this.stamina = Gladiator.DEFAULT_STAMINA;
  }

  // dead code
  public void act() {
    // pour l'instant on attaque. plus tard dodge, avancer etc...
  }

  // dead code, now we use Fight.dealDamage(Gladiator, Gladiator)
  public void attack(Gladiator otherGlad) {
    otherGlad.removeHP(10);

    String message = MessageFormat.format("{0}, {1} frappes {2} pour 10 HP!",
        Resources.getInstance().getRandomSound(), this.getNomen(), otherGlad.getNomen());
    System.out.println(message);
  }

  /**
   * train(String abilityToTrain)
   * 
   * @param abilityToTrain
   */
  public void train(String abilityToTrain) {
    switch (abilityToTrain) {
      case "STR":
        this.strength += GRandom.between(5, 7);
        break;

      case "STAM":
        this.stamina += GRandom.between(10, 15);
        break;

      case "DEF":
        this.defense += GRandom.between(5, 10);
        break;

      default:
        // error
        break;
    }
  }

  @Override
  public String toString() {
    return "Gladiator [nomen=" + nomen + ", HP=" + hp + "]";
  }

}
