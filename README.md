# projet Gladiator

Simulation de gladiateurs, les combats sont résolus automatiquement et le joueur va principalement 
s'occuper de la stratégie d'entrainement, etc de son gladiateur.

## Organisation
3 principales classes :
- `Story`
- `Fight`
- `Gladiator`

### Story

contient le déroulement du jeu, gère l'écriture dans la console et l'input utilisateur (`Scanner`).

### Fight

organise les combats entre les gladiateurs, et gère Health et Stamina

### Gladiator

représente un gladiateur avec ses différents attributs. Pour l'instant l'entrainement (`train()`) est dans cette classe.

## Brief
### Projet Jeu

L'objectif de ce projet est de réaliser un petit jeu via de la POO qui sera jouable en ligne de commande (par exemple)

### Organisation
1. Créer un gitlab dans lequel vous mettrez le jeu
2. Initialiser un projet Java maven et le lier à votre gitlab
3. Choisir un jeu (exemple de jeu plus bas pour les gens qui n'ont pas d'idée)
4. Commencer à refléchir aux classes et méthode dont on aura besoin pour le jeu, pour ça on peut essayer de se demander "quelles sont les actions possibles sur mon jeu ?"
5. On peut essayer les actions en appelant directement les méthodes dans le main avant de rajouter l'interactivité avec la ligne de commande (ou Swing ou ce que vous voulez en vrai, mais l'objectif principale c'est que le jeu fonctionne, qu'il soit interactif ou non)


### Idées de jeux
* Wordle
* Un morpion
* Un puissance 4
* Un sudoku ?
* Un memory
* Une bataille navale
* Un tamagochi
* Un mini rpg (gestion de personnage, d'équipement, d'interaction avec d'autres personnages, de placement sur une grille, etc.)
